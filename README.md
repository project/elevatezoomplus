
# ElevateZoomPlus
Integrates ElevateZoom Plus with Slick or Splide and lightboxes.

***
## <a name="first"> </a>FIRST THINGS FIRST!
Read more at:
* [Github](https://git.drupalcode.org/project/blazy/-/blob/3.0.x/docs/README.md#first-things-first)
* [Blazy UI](/admin/help/blazy_ui#first)


## REQUIREMENTS
1. [Splide 2.x](https://drupal.org/project/splide), or
   [Slick 3.x](https://drupal.org/project/slick). Only one of them.
2. [Blazy 3.x](https://drupal.org/project/blazy) 2.5+ for compatibility between
   Slick and Splide.
3. ElevateZoomPlus library:
   * [Download ElevateZoomPlus](https://github.com/igorlino/elevatezoom-plus)
   * Extract it as is, rename it to **elevatezoom-plus**, so the assets are at:  
     + **/libraries/elevatezoom-plus/src/jquery.ez-plus.js**

     If using Composer it will be:
     + **/libraries/ez-plus/src/jquery.ez-plus.js**

     Both are supported.
4. If using Splide/ Slick with asNavFor/ navigation, any lightbox
   for **Media switcher**. If not, requires **Image to Elevatezoomplus**.


## INSTALLATION
Install the module as usual, more info can be found on:

[Installing Drupal 8 Modules](https://drupal.org/node/1897420)

## USAGE
1. Install ElevateZoomPlus UI, visit **/admin/config/media/elevatezoomplus**.
2. Use blazy-related formatters: Blazy, Slick, Splide, GridStack, etc.
3. Add an **ElevateZoomPlus** optionset, image styles and a lightbox.


### USAGE VARIANTS
1. Splide/ Slick **with** asNavFor integrates with any lightbox, recommended.
2. Splide/ Slick **without** asNavFor require:  
   + Splide `perPage > 1` or Slick `slidesToShow > 1`.
   + **Image to ElevateZoomPlus** under **Media switcher**
2. Blazy Grid, Gridstack, etc. require:  
   + **Image to ElevateZoomPlus** under **Media switcher**   

With **Image to ElevateZoomPlus**, no lightbox behavior, just a full screen
video if available. Use Blazy grid for thumbnails if not using carousels.


### IMAGE STYLES
We don't add more image styles, instead re-using existing ones regardless names:

1. Splide/ Slick **with** asNavFor:
   1. **Image style** for the main preview image (visible one at a time).
   2. **Thumbnail style** for the gallery thumbnails (visible multiple).

2. Splide/ Slick **without** asNavFor, Blazy Grid, Gridstack, etc.:
   1. **Image style** for the gallery thumbnails (visible multiple).
   2. **Thumbnail style** for the main preview image (visible one at a time).

And both uses **Lightbox image style** for the largest zoomed image.

_Note:_
**Visible one at a time**: Splide `perPage = 1`, or Slick `slidesToShow = 1`.  
**Visible multiple**: Splide `perPage > 3`, or Slick `slidesToShow > 3`.

**Important!**

If using Splide/ Slick with asNavFor, you can choose any lightbox.
If not, just a static stage, be sure to choose **Image to ElevateZoomPlus**
to sync the main preview and its thumbnails if you don't use carousels.

For the library usages, please consult their documentations. This module only
provides and facilitates integration.

## CONFUSING IMAGE STYLES
We understand the confusing image styles due to supporting various modules
outlined above. Easy solutions, create 3 dedicated image styles, says:
* `EZ Thumb` (210x210), for thumbnails, any aspect ratio.
* `EZ Stage` (1024x768), for the main stage, aspect ratio 4:3.
* `EZ Zoom` (1600x1200), for the zoom images, similar ratio to `EZ Stage`.

Then map them to the above outlined `IMAGE STYLES` section.
Different aspect ratios may cause issues: collapsed zoom images, etc.

When selecting the image styles, just choose the correct names.
See [Blazy UI help](/admin/help/blazy_ui#aspect-ratio-template) for aspect
ratio samples.

## FEATURES
* Has no formatter, instead integrated into **Media switcher** option as seen at
  Blazy, Splide, Slick, Gridstack formatters.
* Thumbnail gallery thanks to Splide/ Slick asNavFor.  
* Supports video as a full screen video, if using **Image to ElevateZoomPlus**.
* Integrates with any lightbox supported by Blazy if using Splide/ Slick with
  asNavFor.


## KNOWN ISSUES/ LIMITATIONS/ TROUBLESHOOTINGS
* ElevateZoomPlus is not a lightbox, but treated so at Blazy internally. Do not
  expect regular lightbox features like captions, etc.
* Not tested with all blazy-related features/ formatters, yet.
* Best with similar aspect ratio.
* Use Blazy lazyload for Splide/ Slick, if any issue. Edit the working
  Optionset, and change its **Lazy load** option to **Blazy**.
* Not compatible with default Belgrade theme due to highly customized field
  templates, theme-specific goals, please start with core Bartik for initial
  debug works instead.

## MIGRATING FROM SLICK TO SPLIDE, AND VICE VERSA
Just change the formatters, and likely default image styles.

## SIMILAR MODULES
* [Imagezoom](https://drupal.org/project/imagezoom)

  The main difference is it has nothing to do with blazy-related modules,
  and has its own gallery. While this module makes use of Splide/ Slick.


## AUTHOR/MAINTAINER/CREDITS
* [Gaus Surahman](https://drupal.org/user/159062)
* [Committers](https://www.drupal.org/node/3039369/committers)
* CHANGELOG.txt for helpful souls with their patches, suggestions and reports.


## READ MORE
See the project page on drupal.org:

[ElevateZoomPlus module](https://drupal.org/project/elevatezoomplus)

See the ElevateZoomPlus JS docs at:

* [ElevateZoomPlus website](https://igorlino.github.io/elevatezoom-plus/)
* [ElevateZoomPlus at Github](https://github.com/igorlino/elevatezoom-plus)
