<?php

namespace Drupal\elevatezoomplus;

use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Provides common elevatezoomplus utility methods.
 */
interface ElevateZoomPlusManagerInterface extends TrustedCallbackInterface {

  /**
   * Returns Blazy manager service.
   */
  public function manager();

  /**
   * Checks if the requirements are met.
   */
  public function isApplicable(array $settings): bool;

  /**
   * Return the options for the JSON object.
   *
   * @todo remove some more settings fallback post Blazy 2.16.
   */
  public function getOptions(array $settings): array;

  /**
   * Returns available options for select options.
   *
   * @todo remove if BlazyManager has it.
   */
  public function getOptionsetOptions($entity_type): array;

  /**
   * The #pre_render callback: Provides ElevateZoomPlus related contents.
   */
  public function preRenderBuild(array $element): array;

  /**
   * Overrides variables for theme_blazy().
   *
   * @todo remove some more settings fallback post Blazy 2.16.
   */
  public function preprocessBlazy(array &$variables): void;

  /**
   * Implements hook_blazy_attach_alter().
   */
  public function attachAlter(array &$load, array $attach): void;

  /**
   * Takes over original pre_render to avoid working with early render.
   */
  public function buildAlter(array &$build, array $settings): void;

  /**
   * Implements hook_blazy_form_element_alter().
   */
  public function formElementAlter(array &$form, array $definition): void;

  /**
   * Implements hook_library_info_alter().
   */
  public function libraryInfoAlter(array &$libraries, $extension): void;

}
