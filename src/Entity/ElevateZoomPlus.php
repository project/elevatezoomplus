<?php

namespace Drupal\elevatezoomplus\Entity;

use Drupal\blazy\Config\Entity\BlazyConfigEntityBase;

/**
 * Defines the ElevateZoomPlus configuration entity.
 *
 * @ConfigEntityType(
 *   id = "elevatezoomplus",
 *   label = @Translation("ElevateZoomPlus optionset"),
 *   list_path = "admin/config/media/elevatezoomplus",
 *   config_prefix = "optionset",
 *   entity_keys = {
 *     "id" = "name",
 *     "label" = "label",
 *     "status" = "status",
 *     "weight" = "weight",
 *   },
 *   config_export = {
 *     "id",
 *     "name",
 *     "weight",
 *     "label",
 *     "options",
 *   }
 * )
 * @phpstan-ignore-next-line
 */
class ElevateZoomPlus extends BlazyConfigEntityBase implements ElevateZoomPlusInterface {}
