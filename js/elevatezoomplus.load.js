/**
 * @file
 * Provides ElevateZoomPlus loader.
 */

(function ($, Drupal, drupalSettings, _db) {

  'use strict';

  var ID = 'elevatezoomplus';
  var ID_ONCE = ID;
  var NICK = 'ezPlus';
  var WRAPPER = ID + '-wrapper';
  var MOUNTED = WRAPPER + '--on';
  var S_ELEMENT = '.' + WRAPPER + ':not(.' + MOUNTED + ')';
  var S_ZOOM_CONTAINER = '.ZoomContainer';

  /**
   * ElevateZoomPlus public methods.
   *
   * @namespace
   */
  Drupal.elevateZoomPlus = $.extend({
    itemSelector: '.' + ID,
    defaults: drupalSettings.elevateZoomPlus || {},
    ez: null
  }, Drupal.elevateZoomPlus || {});

  /**
   * ElevateZoomPlus gallery item functions.
   *
   * @param {HTMLElement} elm
   *   The .elevatezoomplus-wrapper HTML element.
   */
  function process(elm) {
    var me = Drupal.elevateZoomPlus;
    var options = me.defaults;
    var selector = me.itemSelector;
    var $elm = $(elm);
    var $target = $(selector, elm);
    var $slick = $('.slick--main', elm);
    var $splide = $('.splide--main', elm);
    var $slider = $('> .slick__slider', $slick);
    var dataConfig = $elm.data(ID) || {};
    var isSlick = $slick.length;
    var currSlide = isSlick
      ? '.slick-current ' + selector
      : '.slide.is-active ' + selector;

    // Bail out if we have no config available.
    if (!dataConfig) {
      return;
    }

    // Attach our behaviors.
    options = $.extend({}, options, dataConfig);

    // Integrates with Splide/ Slick with asNavFor.
    if (!$slider.length) {
      $slider = $splide;
    }

    /**
     * Closes the zoom window, can only exist one at a time.
     */
    function zoomOut() {
      if (me.ez) {
        me.ez.closeAll();
      }
      if ($(S_ZOOM_CONTAINER).length) {
        $(S_ZOOM_CONTAINER).remove();
      }
    }

    /**
     * Zooms in the image, can only exist one at a time.
     */
    function zoomIn() {
      zoomOut();

      // DOM cleanups fix.
      setTimeout(function () {
        $target = $(currSlide, $slider);
        $target.ezPlus(options);

        me.ez = $target.data(NICK);
      });
    }

    // Sliders with thumbnail navigation.
    if ($slider.length) {
      zoomIn();

      if (isSlick) {
        $slider.on('beforeChange.ez', zoomOut);
        $slider.on('afterChange.ez', zoomIn);
      }
      else {
        var splide = $slider[0].splide;

        if (splide) {
          splide.on('moved.ez', zoomOut);
          splide.on('active.ez', zoomIn);
        }
      }
    }
    else {
      // Sliders without navigation, Blazy Grid, Gridstack, etc.
      $target.ezPlus(options);
      me.ez = $target.data(NICK);
    }

    $elm.addClass(MOUNTED);
  }

  /**
   * Attaches ElevateZoomPlus behavior to HTML element.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.elevateZoomPlus = {
    attach: function (context) {
      _db.once(process, ID_ONCE, S_ELEMENT, context);
    },
    detach: function (context, settings, trigger) {
      if (trigger === 'unload') {
        _db.once.removeSafely(ID_ONCE, S_ELEMENT, context);
      }
    }
  };

}(jQuery, Drupal, drupalSettings, dBlazy));
