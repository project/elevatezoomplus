<?php

/**
 * @file
 * Hooks and preprocess functions for the Blazy module.
 */

use Drupal\Component\Serialization\Json;
use Drupal\blazy\Blazy;

/**
 * Prepares variables for elevatezoomplus.html.twig templates.
 *
 * @see Drupal\commerce_product\ProductVariationFieldRenderer
 */
function template_preprocess_elevatezoomplus(array &$variables) {
  $element = $variables['element'];

  foreach (['attributes', 'content', 'settings'] as $key) {
    $variables[$key] = $element['#' . $key] ?? [];
  }

  $attributes = &$variables['attributes'];
  $settings = $variables['settings'];
  $blazies = $settings['blazies'];
  $json = elevatezoomplus()->getOptions($settings);
  $nav = $settings['nav'] ?? FALSE;
  $nav = $blazies->is('nav', $nav);

  // Remove problematic container class which cause multiple AJAX classes due
  // to using the same attributes array affecting this theme.
  $class = $settings['ajax_replace_class'] ?? '';
  if ($class && isset($attributes['class'])) {
    $attributes['class'] = array_diff((array) $attributes['class'], [$class]);
  }

  $attributes['data-elevatezoomplus'] = Json::encode($json);
  $variables['content_attributes'] = ['data-blazy' => ''];

  // Checks if Splide/ Slick asNavFor, or regular carousels, Blazy Grid, etc.
  // Only provides static stage if not using Splide/Slick asNavFor.
  $variables['stage'] = $nav ? [] : elevatezoomplus_build($settings);
}

/**
 * Build the main gallery image/ stage if not using Slick asNavFor.
 */
function elevatezoomplus_build(array $settings) {
  $obj   = $settings['blazies']->reset($settings);
  $uri   = $obj->get('first.uri');
  $item  = $obj->get('first.item');
  $box   = $obj->get('box.style');
  $url   = Blazy::transformRelative($uri, $box);
  $style = $settings['thumbnail_style'] ?? '';
  $param = ['image_style' => $style, 'ratio' => 'fluid', 'blazy' => TRUE];
  $id    = blazy()->getHtmlId('elevatezoomplus');
  $stage = $obj->get('thumbnail.style');
  $tnid  = $obj->get('thumbnail.id');
  $attrs = [
    'class' => [
      'elevatezoomplus',
      'elevatezoomplus--stage',
    ],
    'id' => $id,
    'data-zoom-image' => $url,
  ];

  // Interpolate thumbnail_style as stage to the image_style.
  $obj->set('image.style', $stage)
    ->set('image.id', $tnid)
    ->set('image.uri', $uri)
    // Set TRUE, if you want to link to the zoom image, or don't set it.
    ->set('is.lightbox', FALSE);

  $info = [
    'width'  => $obj->get('image.width'),
    'height' => $obj->get('image.height'),
    'uri'    => $uri,
  ];

  if ($stage) {
    $data = Blazy::transformDimensions($stage, $info);
    $obj->set('image', $data, TRUE);
  }

  // @todo enable at blazy:3.x:
  // ->set('image.attributes', $attrs, TRUE);
  $sets = $param;
  $sets['blazies'] = $obj;

  $build = [
    '#delta' => 0,
    '#item' => $item,
    '#settings' => $sets,
    '#item_attributes' => $attrs,
  ];

  return [
    '#type'     => 'inline_template',
    '#template' => '{{ stage }}',
    '#context'  => ['stage' => blazy()->getBlazy($build)],
    '#attached' => blazy()->attach($param + $settings),
  ];
}
